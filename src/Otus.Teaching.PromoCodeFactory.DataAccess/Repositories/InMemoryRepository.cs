﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected List<T> Data { get; set; }


        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<T> CreateAsync(T newValue)
        {
            newValue.Id = Guid.NewGuid();

            await Task.Run(() => Data.Add(newValue));
            return newValue;
        }

        public async Task<T> UpdateAsync(Guid id, T updateValue)
        {
            var index = await Task.Run(() => Data.FindIndex(0, 1, (x) => x.Id == id));

            if (index == -1)
            {
                throw new Exception("Элемент с таким ID не найден.");
            }

            updateValue.Id = id;
            Data[index] = updateValue;
            return updateValue;
        }

        public async Task DeleteAsync(Guid id)
        {
            var index = await Task.Run(() => Data.FindIndex(0, 1, (x) => x.Id == id));

            if (index == -1)
            {
                throw new Exception("Элемент с таким ID не найден.");
            }

            Data.RemoveAt(index);
        }
    }
}