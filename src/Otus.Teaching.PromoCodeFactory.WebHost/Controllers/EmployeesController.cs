﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeRolesTasks = employee.RolesIds.Select(x => _rolesRepository.GetByIdAsync(x));
            await Task.WhenAll(employeeRolesTasks);
            var employeeRoles = employeeRolesTasks.Select(x => x.Result).Where(x => x != null);
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employeeRoles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }
                ).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника.
        /// </summary>
        [HttpPut]
        public async Task CreateEmployee(EmployeeRequest employeeRequest)
        {
            var newEmployee = new Employee()
            {
                Id = default,
                FirstName = employeeRequest.Name,
                LastName = employeeRequest.SecondName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                RolesIds = employeeRequest.RoleIds
            };

            await _employeeRepository.CreateAsync(newEmployee);
        }

        /// <summary>
        /// Обновить сотрудника.
        /// </summary>
        [HttpPost("{id:guid}")]
        public async Task UpdateEmployee(Guid id, EmployeeRequest employeeRequest)
        {
            var newEmployee = new Employee()
            {
                Id = id,
                FirstName = employeeRequest.Name,
                LastName = employeeRequest.SecondName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                RolesIds = employeeRequest.RoleIds
            };

            await _employeeRepository.UpdateAsync(id, newEmployee);
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task DeleteEmployee(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);
        }
    }
}