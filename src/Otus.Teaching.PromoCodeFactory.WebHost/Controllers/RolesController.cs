﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Создать роль.
        /// </summary>
        [HttpPut]
        public async Task CreateRole(RoleRequest roleRequest)
        {
            var newRole = new Role()
            {
                Id = default,
                Name = roleRequest.Name,
                Description = roleRequest.Description
            };

            await _rolesRepository.CreateAsync(newRole);
        }

        /// <summary>
        /// Обновить роль.
        /// </summary>
        [HttpPost("{id:guid}")]
        public async Task UpdateRole(Guid id, RoleRequest roleRequest)
        {
            var newRole = new Role()
            {
                Id = default,
                Name = roleRequest.Name,
                Description = roleRequest.Description
            };

            await _rolesRepository.UpdateAsync(id, newRole);
        }

        /// <summary>
        /// Удалить роль.
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task DeleteRole(Guid id)
        {
            await _rolesRepository.DeleteAsync(id);
        }
    }
}