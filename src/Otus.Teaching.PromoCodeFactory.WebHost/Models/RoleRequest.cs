﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}